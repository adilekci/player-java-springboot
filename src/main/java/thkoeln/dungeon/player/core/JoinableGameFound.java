package thkoeln.dungeon.player.core;

import java.util.UUID;

public record JoinableGameFound(
    UUID gameId
) {

}
